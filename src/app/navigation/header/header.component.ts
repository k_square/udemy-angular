import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Observable } from 'rxjs';

import { Store } from '@ngrx/store';
import * as fromRoot from '../../app.reducer';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isAuth: boolean;

  // authSubscription: Subscription;

  @Output() sideNavToggle = new EventEmitter<void>();

  isAuth$: Observable<boolean>;

  constructor(private authService: AuthService,
    private store: Store<fromRoot.State>) { }

  ngOnInit() {
    // this.authService.authChange.subscribe(authStatus => {
    //   this.isAuth = authStatus;
    // });
    this.isAuth$ = this.store.select(fromRoot.getIsAuth);
  }

  // ngOnDestroy() {
  //   this.authSubscription.unsubscribe();
  // }

  onLogout() {
    this.authService.logout();
  }

  onToggleSideNav() {
    this.sideNavToggle.emit();
  }

}
