import { Component, OnInit, OnDestroy } from '@angular/core';
import { TrainingService } from '../training.service';
import { Exercise } from '../exercise.model';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { UIService } from '../../shared/ui.service';
import { Store } from '@ngrx/store';
import * as fromTraining from '../training.reducer';
import * as fromRoot from '../../app.reducer';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.css']
})
export class NewTrainingComponent implements OnInit, OnDestroy {

  exerciceLoaded = false;

  exercises$: Observable<Exercise[]>;

  // exerciseSubscription: Subscription;

  // private loadingSubscription: Subscription;

  isLoadingState$: Observable<boolean>;

  constructor(
    private trainingService: TrainingService,
    private db: AngularFirestore,
    private uiService: UIService,
    private store: Store<fromTraining.State>
  ) { }


  ngOnInit() {
    // this.loadingSubscription = this.uiService.loadingStateChanged.subscribe(
    //   isLoading => this.exerciceLoaded = !isLoading
    // );
    this.isLoadingState$ = this.store.select(fromRoot.getIsLoading);
    this.exercises$ = this.store.select(fromTraining.getAvailableExercises);
    // this.exerciseSubscription = this.trainingService.exercicesChanged.subscribe(
    //   ex => {
    //     this.exercises = ex;
    //     // this.exerciceLoaded = true;
    //   }
    // );
    this.fetchExercises();
  }

  ngOnDestroy() {
    // if (this.exerciseSubscription) {
    //   this.exerciseSubscription.unsubscribe();
    // }
    // if (this.loadingSubscription) {
    //   this.loadingSubscription.unsubscribe();
    // }
  }

  onStartTraining(form: NgForm) {
    // console.log(form.value.exercise);
    this.trainingService.startExercise(form.value.exercise);
  }

  fetchExercises() {
    this.trainingService.fetchAvailableExercises();
  }

}
