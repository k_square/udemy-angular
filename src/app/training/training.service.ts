import { Exercise } from './exercise.model';
import { Subject, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { map, take } from 'rxjs/operators';
import { UIService } from '../shared/ui.service';
// import * as fromRoot from '../app.reducer';
import * as UI from '../shared/ui.action';
import * as fromTraining from './training.reducer';
import * as Training from './training.actions';
import { Store } from '@ngrx/store';


@Injectable()
export class TrainingService {

    // exerciseChanged = new Subject<Exercise>();

    // exercicesChanged = new Subject<Exercise[]>();

    // finishedExercisesChanged = new Subject<Exercise[]>();

    // availableExercise: Exercise[];

    // private runningExercise: Exercise;

    // private exercises: Exercise[] = [];

    private fbSubs: Subscription[] = [];

    constructor(
        private db: AngularFirestore,
        private uiService: UIService,
        private store: Store<fromTraining.State>
    ) { }

    fetchAvailableExercises() {
        // this.uiService.loadingStateChanged.next(true);
        this.store.dispatch(new UI.StartLoading());
        this.fbSubs.push(this.db.collection<Exercise>('availableExercises')
            .snapshotChanges()
            .pipe(
                map(actions => actions.map(a => {

                    return {
                        id: a.payload.doc.id,
                        name: a.payload.doc.data().name,
                        duration: a.payload.doc.data().duration,
                        calories: a.payload.doc.data().calories
                        // date: a.payload.doc.data().date as Date
                    };
                }))
            ).subscribe((exercises: Exercise[]) => {
                // this.uiService.loadingStateChanged.next(false);
                this.store.dispatch(new UI.StopLoading());
                this.store.dispatch(new Training.SetAvailableTrainings(exercises));
                // this.availableExercise = exercises;
                // this.exercicesChanged.next([...this.availableExercise]);
            }, error => {
                // this.uiService.loadingStateChanged.next(false);
                this.store.dispatch(new UI.StopLoading());
                this.uiService.showSnackBar('Fetching Exercises failed, please...', null, 2000);
                // this.exercicesChanged.next(null);
            }));
    }

    // getAvailableExercise() {
    //     return this.availableExercise.slice();
    // }

    startExercise(selectedId: string) {
        // this.db.doc('availableExercises/' + selectedId).update(
        //     { lastSelected: new Date() }
        // );
        // this.runningExercise = this.availableExercise.find(ex => ex.id === selectedId);
        // this.exerciseChanged.next({ ...this.runningExercise });
        this.store.dispatch(new Training.StartTraining(selectedId));
    }

    completeExercise() {
        this.store.select(fromTraining.getActiveTraining).pipe(take(1)).subscribe(ex => {
            this.addDataToDatabase({
                ...ex,
                date: new Date(),
                state: 'completed'
            });
        });
        this.store.dispatch(new Training.StopTraining());
    }

    cancelExercise(progress: number) {
        this.store.select(fromTraining.getActiveTraining).pipe(take(1)).subscribe(ex => {
            this.addDataToDatabase({
                ...ex,
                duration: ex.duration * (progress / 100),
                calories: ex.calories * (progress / 100),
                date: new Date(),
                state: 'cancelled'
            });
        });
        this.store.dispatch(new Training.StopTraining());
    }


    fetchCompletedOrCanceledExercises() {
        this.fbSubs.push(this.db.collection('finishedExercices')
            .valueChanges()
            .subscribe(
                (exercises: Exercise[]) => {
                    this.store.dispatch(new Training.SetFinishedTrainings(exercises));
                }
            ));
    }

    cancelSubscription() {
        this.fbSubs.forEach(sub => sub.unsubscribe());
    }

    private addDataToDatabase(exercise: Exercise) {
        this.db.collection('finishedExercices').add(exercise);
    }
}
