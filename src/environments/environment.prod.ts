export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBemWVHAyFxw_9vYzePV2h-GL7qjbybTkQ',
    authDomain: 'fitness-tracker-1.firebaseapp.com',
    databaseURL: 'https://fitness-tracker-1.firebaseio.com',
    projectId: 'fitness-tracker-1',
    storageBucket: 'fitness-tracker-1.appspot.com',
    messagingSenderId: '763887306043'
  }
};
